export class Adresse {
    public plz: string;

    public ort: string;

    constructor(plz: string, ort: string) {
        this.plz = plz;
        this.ort = ort;
    }
}

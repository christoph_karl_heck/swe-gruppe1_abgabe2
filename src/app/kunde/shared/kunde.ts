/* eslint-disable max-lines */

// eslint-disable-next-line @typescript-eslint/consistent-type-imports
import { Adresse } from './adresse';
// eslint-disable-next-line @typescript-eslint/consistent-type-imports
import { Umsatz } from './umsatz';

/*
 * Copyright (C) 2015 - present Juergen Zimmermann, Hochschule Karlsruhe
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <http://www.gnu.org/licenses/>.
 */

export type Familienstand = 'VH' | 'L' | 'G' | 'VW';

export type KundeGeschlecht = 'W' | 'M' | 'D';

export type Interessen = 'L' | 'R' | 'S';

// eslint-disable-next-line unicorn/no-unsafe-regex
export const ORT_REGEX = /^[a-zA-Z]+(?:[\s-][a-zA-Z]+)*$/u;

/**
 * Gemeinsame Datenfelder unabh&auml;ngig, ob die Kundedaten von einem Server
 * (z.B. RESTful Web Service) oder von einem Formular kommen.
 */
export interface KundeShared {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _id?: string;
    nachname: string | undefined;
    email: string;
    kategorie: number | undefined;
    newsletter?: boolean;
    geburtsdatum: Date;
    umsatz?: Umsatz;
    homepage: string;
    geschlecht: KundeGeschlecht;
    familienstand?: Familienstand | '';
    interessen: string[] | undefined;
    adresse: Adresse;
    username?: string;
    version?: number;
}

interface Link {
    href: string;
}

/**
 * Daten vom und zum REST-Server:
 * <ul>
 *  <li> Arrays f&uuml;r mehrere Werte, die in einem Formular als Checkbox
 *       dargestellt werden.
 *  <li> Daten mit Zahlen als Datentyp, die in einem Formular nur als
 *       String handhabbar sind.
 * </ul>
 */

export interface KundeServer extends KundeShared {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _links?: {
        self: Link;
        list?: Link;
        add?: Link;
        update?: Link;
        remove?: Link;
    };
}

/**
 * Daten aus einem Formular:
 * <ul>
 *  <li> je 1 Control fuer jede Checkbox und
 *  <li> au&szlig;erdem Strings f&uuml;r Eingabefelder f&uuml;r Zahlen.
 * </ul>
 */

/**
 * Model als Plain-Old-JavaScript-Object (POJO) fuer die Daten *UND*
 * Functions fuer Abfragen und Aenderungen.
 */
export class Kunde {
    private static readonly SPACE = 2;

    // geburtsdatum: Date | undefined;

    // waehrung: number | undefined;

    // wird aufgerufen von fromServer() oder von fromForm()
    // eslint-disable-next-line max-params
    private constructor(
        public _id: string | undefined, // eslint-disable-line @typescript-eslint/naming-convention
        public nachname: string,
        public email: string,
        public kategorie: number,
        public newsletter: boolean | undefined,
        public geburtsdatum: Date,
        public homepage: string,
        public geschlecht: KundeGeschlecht,
        public familienstand: Familienstand | undefined | '',
        public interessen: string[] | undefined,
        public adresse: Adresse,
        public username: string | undefined,
        public version: number | undefined,
        public umsatz?: Umsatz,
    ) {
        // TODO Parsing, ob der Geburtsdatum-String valide ist
        this.geburtsdatum = new Date(geburtsdatum);
        console.log('Kunde(): this=', this);
    }

    /**
     * Ein Kunde-Objekt mit JSON-Daten erzeugen, die von einem RESTful Web
     * Service kommen.
     * @param kunde JSON-Objekt mit Daten vom RESTful Web Server
     * @return Das initialisierte Kunde-Objekt
     */
    static fromServer(kundeServer: KundeServer, etag?: string) {
        console.log('debug', kundeServer);
        console.log(typeof kundeServer);
        let selfLink: string | undefined;
        const { _links } = kundeServer; // eslint-disable-line @typescript-eslint/naming-convention
        if (_links !== undefined) {
            const { self } = _links;
            selfLink = self.href;
        }
        let id: string | undefined;
        if (selfLink !== undefined) {
            const lastSlash = selfLink.lastIndexOf('/');
            id = selfLink.slice(lastSlash + 1);
        }

        let version: number | undefined;
        if (etag !== undefined) {
            // Anfuehrungszeichen am Anfang und am Ende entfernen
            const versionStr = etag.slice(1, -1);
            version = Number.parseInt(versionStr, 10);
        }

        const kunde = new Kunde(
            id,
            kundeServer.nachname ?? 'unbekannt',
            kundeServer.email,
            kundeServer.kategorie ?? 0,
            kundeServer.newsletter,
            kundeServer.geburtsdatum,
            kundeServer.homepage,
            kundeServer.geschlecht,
            kundeServer.familienstand,
            kundeServer.interessen,
            kundeServer.adresse,
            kundeServer.username,
            version,
            kundeServer.umsatz,
        );
        console.log('debug2', kunde.geschlecht, kunde.familienstand);
        console.log('Kunde.fromServer(): kunde=', kunde);
        return kunde;
    }

    /**
     * Ein Kunde-Objekt mit JSON-Daten erzeugen, die von einem Formular kommen.
     * @param kunde JSON-Objekt mit Daten vom Formular
     * @return Das initialisierte Kunde-Objekt
     */
    static fromForm(kundeForm: KundeShared) {
        console.log('Kunde.fromForm(): kundeForm=', kundeForm);

        const kategorie =
            kundeForm.kategorie === undefined ? 0 : kundeForm.kategorie / 100; // eslint-disable-line @typescript-eslint/no-magic-numbers
        const kunde = new Kunde(
            kundeForm._id,
            kundeForm.nachname ?? 'unbekannt',
            kundeForm.email,
            kategorie,
            kundeForm.newsletter,
            kundeForm.geburtsdatum,
            kundeForm.homepage,
            kundeForm.geschlecht,
            kundeForm.familienstand,
            kundeForm.interessen,
            kundeForm.adresse,
            kundeForm.username,
            kundeForm.version,
            kundeForm.umsatz,
        );
        console.log('Kunde.fromForm(): kunde=', kunde);
        return kunde;
    }

    // Property in TypeScript wie in C#
    // https://www.typescriptlang.org/docs/handbook/classes.html#accessors
    get geburtsdatumFormatted() {
        // z.B. 7. Mai 2020
        const formatter = new Intl.DateTimeFormat('de', {
            year: 'numeric',
            month: 'long',
            day: 'numeric',
        });
        return formatter.format(this.geburtsdatum);
    }

    /**
     * Abfrage, ob im Kundenachname der angegebene Teilstring enthalten ist. Dabei
     * wird nicht auf Gross-/Kleinschreibung geachtet.
     * @param nachname Zu &uuml;berpr&uuml;fender Teilstring
     * @return true, falls der Teilstring im Kundenachname enthalten ist. Sonst
     *         false.
     */
    containsNachname(nachname: string) {
        return this.nachname.toLowerCase().includes(nachname.toLowerCase());
    }

    /**
     * Die Bewertung ("betrag") des Kundees um 1 erh&ouml;hen
     */

    /**
     * Abfrage, ob das Kunde dem angegebenen Familienstand zugeordnet ist.
     * @param familienstand der Name des Familienstands
     * @return true, falls das Kunde dem Familienstand zugeordnet ist. Sonst false.
     */
    hasFamilienstand(familienstand: string) {
        return this.familienstand === familienstand;
    }

    /**
     * Aktualisierung der Stammdaten des Kunde-Objekts.
     * @param nachname Der neue Kundenachname
     * @param betrag Die neue Bewertung
     * @param geschlecht Die neue Kundegeschlecht (MAENNLICH, WEIBLICH oder DIVERS)
     * @param familienstand Der neue Familienstand
     * @param plz Der neue Plz
     * @param kategorie Der neue Kategorie
     */
    // eslint-disable-next-line max-params
    updateStammdaten(
        nachname: string,
        geschlecht: KundeGeschlecht,
        familienstand: Familienstand | undefined | '',
        geburtsdatum: Date,
        plz: string,
        kategorie: number,
        ort: string,
    ) {
        this.nachname = nachname;
        this.geschlecht = geschlecht;
        this.familienstand = familienstand;
        this.geburtsdatum = geburtsdatum;
        this.adresse.plz = plz;
        this.kategorie = kategorie;
        this.adresse.ort = ort;
    }

    /**
     * Abfrage, ob es zum Kunde auch Schlagw&ouml;rter gibt.
     * @return true, falls es mindestens ein Schlagwort gibt. Sonst false.
     */
    hasInteressen() {
        if (this.interessen === undefined) {
            return false;
        }
        return this.interessen.length !== 0;
    }

    /**
     * Abfrage, ob es zum Kunde das angegebene Schlagwort gibt.
     * @param schlagwort das zu &uuml;berpr&uuml;fende Schlagwort
     * @return true, falls es das Schlagwort gibt. Sonst false.
     */
    hasSchlagwort(schlagwort: string) {
        if (this.interessen === undefined) {
            return false;
        }
        return this.interessen.includes(schlagwort);
    }

    /**
     * Aktualisierung der Schlagw&ouml;rter des Kunde-Objekts.
     * @param lesen ist das Schlagwort LESEN gesetzt
     * @param reisen ist das Schlagwort REISEN gesetzt
     * @param sport ist das Schlagwort SPORT gesetzt
     */
    updateInteressen(lesen: boolean, reisen: boolean, sport: boolean) {
        this.resetInteressen();
        if (lesen) {
            this.addSchlagwort('LESEN');
        }
        if (reisen) {
            this.addSchlagwort('REISEN');
        }
        if (sport) {
            this.addSchlagwort('SPORT');
        }
    }

    /**
     * Konvertierung des Kundeobjektes in ein JSON-Objekt f&uuml;r den RESTful
     * Web Service.
     * @return Das JSON-Objekt f&uuml;r den RESTful Web Service
     */
    toJSON(): KundeServer {
        console.log(
            `toJson(): geburtsdatum=${this.geburtsdatum.toISOString()}`,
        );
        return {
            _id: this._id, // eslint-disable-line @typescript-eslint/naming-convention
            nachname: this.nachname,
            email: this.email,
            kategorie: this.kategorie,
            newsletter: this.newsletter,
            geburtsdatum: this.geburtsdatum,
            umsatz: this.umsatz,
            homepage: this.homepage,
            geschlecht: this.geschlecht,
            familienstand: this.familienstand,
            interessen: this.interessen,
            adresse: this.adresse,
            username: this.username,
        };
    }

    toString() {
        // eslint-disable-next-line no-null/no-null,unicorn/no-null
        return JSON.stringify(this, null, Kunde.SPACE);
    }

    private resetInteressen() {
        this.interessen = [];
    }

    private addSchlagwort(schlagwort: string) {
        if (this.interessen === undefined) {
            this.interessen = [];
        }
        this.interessen.push(schlagwort);
    }
}
/* eslint-enable max-lines */

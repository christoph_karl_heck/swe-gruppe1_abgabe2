import type { KundeServer } from './kunde';

export class Embedded2 {
    kundeList: KundeServer[];

    constructor(kundeList: KundeServer[]) {
        this.kundeList = kundeList;
    }
}

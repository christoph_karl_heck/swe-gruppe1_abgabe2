import type { Embedded2 } from './embedded2';

export class Embedded {
    // eslint-disable-next-line @typescript-eslint/naming-convention
    _embedded: Embedded2;

    constructor(embedded: Embedded2) {
        // eslint-disable-next-line no-underscore-dangle
        this._embedded = embedded;
    }
}

export class Umsatz {
    public betrag: number | undefined;

    public waehrung: string | undefined;

    constructor(betrag: number, waehrung: string) {
        this.betrag = betrag;
        this.waehrung = waehrung;
    }
}
